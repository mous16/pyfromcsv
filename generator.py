import csv
from os import PathLike
from typing import Generator, Type

from typing_extensions import Self, dataclass_transform


@dataclass_transform()
def generable_from_csv(base_cls: Type) -> Type:
    class Helper(base_cls):
        @classmethod
        def generate_from_csv(
            decorated_cls: Type[Self], filename: PathLike, *args, **kwds
        ) -> Generator[Self, None, None]:
            with open(filename, "r", newline="") as file:
                reader = csv.DictReader(file, *args, **kwds)
                for row in reader:
                    instance = decorated_cls()
                    for key, value in row.items():
                        if hasattr(instance, key):
                            setattr(instance, key, value)
                        else:
                            priv_key = f"_{(base_cls.__qualname__)}__{key}"
                            if hasattr(instance, priv_key):
                                setattr(instance, priv_key, value)

                    yield instance

    return Helper
