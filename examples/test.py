from pyfromcsv.generator import generable_from_csv


@generable_from_csv
class Person:
    def __init__(self):
        self.name = ""
        self.age = 0
        self.__email = ""
        self.__zodiac = ""

    def who_am_i(self):
        print(
            f"name: '{self.name}', age: '{self.age}', email: '{self.__email}', zodiac: '{self.__zodiac}'"
        )


print("Reading from 'people.csv':")
for person in Person.generate_from_csv("people.csv", delimiter=";"):
    person.who_am_i()

print("Reading from 'people_headless.csv':")
for person in Person.generate_from_csv(
    "people_headless.csv", delimiter=";", fieldnames=["name", "age", "email", "address"]
):
    person.who_am_i()
